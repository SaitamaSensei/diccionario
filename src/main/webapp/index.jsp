<%-- 
    Document   : index
    Created on : 10-05-2020, 17:25:27
    Author     : SaitamaSensei
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Diccionario App</title>
    </head>
    <body>
        <nav class="navbar navbar-dark bg-dark justify-content-center">
            <span class="text-justify" style="color: #ffffff " >Examen Final</span>
        </nav><br>   

        <div class="container">
            <form name="form" action="controlador" method="POST">
                <input name="palabra" required id="palabra" type="text" class="form-control" placeholder="Ingresa la Palabra a buscar"><br>
                <div class="container">
                    <button type="submit" name="accion" value="buscar" class="btn btn-primary btn-lg btn-block">Buscar</button>
                </div><br>
            </form>
            <form name="form" action="controlador" method="POST">
                <div class="container">
                    <button type="submit" name="accion" value="ver" class="btn btn-primary btn-lg btn-block">Ver Historico</button>
                </div>
            </form>
        </div>
        <footer class="fixed-bottom">
            <div class="container-fluid text-center bg-dark text-white">
                <span>Curso: Taller de App Empresariales - Profesor: Cesar Cruces - Alumno: Daniel Espíndola</span>
            </div>
        </footer>

    </body>
</html>
