
package root.servicios;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;
import javax.net.ssl.HttpsURLConnection;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import root.persitence.dao.PalabrasDao;
import root.persitence.entities.Palabras;

/**
 *
 * @author SaitamaSensei
 */
@Path("/diccio")
public class DiccionarioLocal {
    
    PalabrasDao dao =new PalabrasDao();
    
    @GET
    @Path("/{palabra}")
    @Produces(MediaType.APPLICATION_JSON)
    
    public String listarTodo(@HeaderParam("app-key") String appkey, @HeaderParam("app-id") String appId, @PathParam("palabra") String palabra) {
        
        List<Palabras> lista = dao.findPalabrasEntities();
        
        System.out.println("palabra : " + palabra);
        System.out.println("app_key : " + appkey);
        System.out.println("app_id : " + appId);
        final String language = "es";
        final String word = palabra;
        final String fields = "definitions";
        final String strictMatch = "true";
        final String word_id = word.toLowerCase();        
        final String urlOxford = "https://od-api.oxforddictionaries.com:443/api/v2/entries/" + language + "/" + word_id + "?" + "fields=" + fields + "&strictMatch=" + strictMatch;
        
        try 
        {
            URL url = new URL(urlOxford);
            HttpsURLConnection urlConnection = (HttpsURLConnection) url.openConnection();
            urlConnection.setRequestProperty("Content-Type", "application/json charset=UTF-8");
            urlConnection.setRequestProperty("app_id", appId);
            urlConnection.setRequestProperty("app_key", appkey);

            
            BufferedReader reader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(), "UTF-8"));
            System.out.println("Respuesta Oxford : " + reader);
            StringBuilder stringBuilder = new StringBuilder();

            String line = null;
            while ((line = reader.readLine()) != null)
            {
                stringBuilder.append(line + "\n");
            }
            System.out.println("Respuesta Oxford second : " + stringBuilder.toString());
            return stringBuilder.toString();
        } 
        catch (Exception e)
        {
            e.printStackTrace();
            return e.toString();
        }
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public String nuevaPalabra(Palabras palabra) {
        dao.create(palabra);
        return "Registro guardado exitosamente";
    }
    
}
