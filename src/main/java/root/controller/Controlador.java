/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.controller;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import org.json.JSONArray;
import org.json.JSONObject;
import root.persitence.dao.PalabrasDao;
import root.persitence.entities.Palabras;

/**
 *
 * @author SaitamaSensei
 */
@WebServlet(name = "Controlador", urlPatterns = {"/controlador"}, initParams = {
    @WebInitParam(name = "Name", value = "Value")})
public class Controlador extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        PalabrasDao dao = new PalabrasDao();
        Palabras pal = new Palabras();
        String palabra = request.getParameter("palabra");
        String accion = request.getParameter("accion");
        List<Palabras> lista = dao.findPalabrasEntities();
        String url = "https://localhost:8080/dicfinal-1.0/api/diccio/";

        Client client = ClientBuilder.newClient();
        WebTarget myResource = client.target("https://diccionarioex.herokuapp.com/" + palabra);
        final String yeison = myResource.request(MediaType.APPLICATION_JSON).header("api-key", "79093f0e5bf0c90dc2bce44dc451f6a7").header("api-id", "f1adc05c").get(String.class);

        switch (accion) {

            case "buscar":
                String definal = null;
                try {

                    JSONObject json = new JSONObject(yeison);
                    JSONArray array = json.getJSONArray("results");
                    JSONObject json1 = array.getJSONObject(0);
                    JSONArray array1 = json1.getJSONArray("lexicalEntries");
                    JSONObject json2 = array1.getJSONObject(0);
                    JSONArray array2 = json2.getJSONArray("entries");
                    JSONObject json3 = array2.getJSONObject(0);
                    JSONArray array3 = json3.getJSONArray("senses");
                    JSONObject json4 = array3.getJSONObject(0);
                    JSONArray array4 = json4.getJSONArray("definitions");

                    definal = array4.getString(0);
                    /*definal = (String) ((JSONArray) ((JSONObject) ((JSONArray) ((JSONObject) ((JSONArray) ((JSONObject) ((JSONArray) ((JSONObject) ((JSONArray) ((JSONObject) parse)
                .get("results")).get(0)).get("lexicalEntries")).get(0)).get("entries"))
                .get(0)).get("senses")).get(0)).get("definitions")).get(0);*/
                } catch (Exception e) {
                    System.out.println(e);
                }
                System.out.println("resultado desde JSON : " + definal);

                pal.setPalabra(palabra);
                if (definal.equals(null)) {
                    pal.setDefinicion("Palabra no encontrada");
                } else {
                    pal.setDefinicion(definal);
                }
                dao.create(pal);

                request.setAttribute("palabra", palabra);
                request.setAttribute("definicion", definal);
                request.setAttribute("lista", lista);
                request.getRequestDispatcher("salida.jsp").forward(request, response);
            case "ver":
                request.setAttribute("lista", lista);
                request.getRequestDispatcher("lista.jsp").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
