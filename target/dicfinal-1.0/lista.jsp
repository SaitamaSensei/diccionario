<%-- 
    Document   : index
    Created on : 10-05-2020, 17:25:27
    Author     : SaitamaSensei
--%>

<%@page import="root.persitence.entities.Palabras"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Palabras> lista = (List<Palabras>) request.getAttribute("lista");
    Iterator<Palabras> iterLista = lista.iterator();
%>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Diccionario App</title>
    </head>
    <body>
        <nav class="navbar navbar-dark bg-dark justify-content-center">
            <span class="text-justify" style="color: #ffffff " >Lista Historico</span>
        </nav><br>   

        <div class="container">
            <table class="table table-striped table-dark">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Palabra</th>
                        <th scope="col">Definicion</th>
                    </tr>
                </thead> 
                <tbody> 
                    <%
                        while (iterLista.hasNext()) {
                            Palabras pal = iterLista.next();
                    %> 
                    <tr>
                        <th scope="row"><%= pal.getId()%></th>
                        <td><%= pal.getPalabra()%></td>
                        <td><%= pal.getDefinicion()%></td>
                    </tr>
                    <%}%>
                </tbody>
            </table>                

            <footer>
                Alumno: Daniel Espindola.
                Profesor: Cesar Cruces.
                Curso: IC107-50.
            </footer>
    </body>
</html>